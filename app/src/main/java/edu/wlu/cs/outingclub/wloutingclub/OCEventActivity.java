package edu.wlu.cs.outingclub.wloutingclub;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by michaelgarcia on 12/8/16.
 */

public class OCEventActivity extends SingleEventActivity {
    private static final String EXTRA_OCEVENT_ID = "edu.wlu.cs.outingclub.wloutingclub.ocevent_id";
    @Override
    protected Fragment createFragment(){
        OCEventFragment f = new OCEventFragment();
        f.setArguments(getIntent().getExtras());
        return f;
    }

    public static Intent newIntent(Context packageContext, int id){
        Intent intent = new Intent(packageContext, OCEventActivity.class);
        intent.putExtra(EXTRA_OCEVENT_ID, id);
        Log.d("id in newIntent", ""+id);
        return intent;
    }
}
