package edu.wlu.cs.outingclub.wloutingclub;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button mOCSignUpButton;
    private Button mEventSignUpButton;
    private Button mCalendarButton;
    private Button mGuidebookButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instantiate our singleton EventLab
        EventLab.get(this);

        mOCSignUpButton = (Button) findViewById(R.id.OC_sign_up);
        mOCSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("http://managementtools3.wlu.edu/OutingClub/Default.aspx?Tab=7"));
                startActivity(i);
            }
        });


        mEventSignUpButton = (Button) findViewById(R.id.OC_event_sign_up);
        mEventSignUpButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(MainActivity.this, OCEventListActivity.class);
                startActivity(i);
            }
        });

        mCalendarButton = (Button) findViewById(R.id.OC_calendar);
        mCalendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, CalendarActivity.class);
                startActivity(i);
            }
        });

        mGuidebookButton = (Button) findViewById(R.id.OC_guidebook);
        mGuidebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, GuidebookActivity.class);
                startActivity(i);
            }
        });
    }
}