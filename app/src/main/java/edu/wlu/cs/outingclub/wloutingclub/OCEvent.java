package edu.wlu.cs.outingclub.wloutingclub;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by michaelgarcia on 12/8/16.
 */

public class OCEvent {
    // JSON Node names
    private static final String JSON_ID = "ID";
    private static final String JSON_TITLE = "Title";
    private static final String JSON_DESCRIPTION = "Description";
    private static final String JSON_DATE = "Date";
    private static final String JSON_START_DATE = "StartDate";
    private static final String JSON_END_DATE = "EndDate";
    private static final String JSON_DEPOSIT_DUE = "DepositDue";
    private static final String JSON_DEPOSIT_DUE_DATE = "DepositDueDate";
    private static final String JSON_FEE_DUE = "FeeDue";
    private static final String JSON_FEE_DUE_DATE = "FeeDueDate";
    private static final String JSON_LIMIT = "Limit";
    private static final String JSON_FULL = "Full";

    private int mId;
    private String mTitle;
    private String mDescription;
    private Date mDate;
    private Date mStartDate;
    private Date mEndDate;
    private String mDepositDue;
    private Date mDepositDueDate;
    private String mFeeDue;
    private Date mFeeDueDate;
    private int mLimit;
    private boolean mFull;

    public OCEvent(){
    }

    public OCEvent(int id){
    }

    public OCEvent( JSONObject json ) throws JSONException {
        mId = json.getInt(JSON_ID);
        if (json.has(JSON_TITLE)) {
            mTitle = json.getString(JSON_TITLE);
            Log.d("OCEvent", mTitle);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");


        mFull = json.getBoolean(JSON_FULL);
        mDescription = new String(json.getString(JSON_DESCRIPTION));
        try {
            mDate = sdf.parse(json.getString(JSON_DATE));
            mStartDate = sdf.parse(json.getString(JSON_START_DATE));
            mEndDate = sdf.parse(json.getString(JSON_END_DATE));
            mDepositDue = json.getString(JSON_DEPOSIT_DUE);
            mDepositDueDate = sdf.parse(json.getString(JSON_DEPOSIT_DUE_DATE));
            mFeeDue = json.getString(JSON_FEE_DUE);
            mFeeDueDate = sdf.parse(json.getString(JSON_FEE_DUE_DATE));
            mLimit = json.getInt(JSON_LIMIT);
        } catch (Exception e){
            e.printStackTrace();
        }


    }

    public JSONObject toJSON() throws JSONException {

        JSONObject json = new JSONObject();

        json.put(JSON_ID, mId);
        json.put(JSON_TITLE, mTitle);
        json.put(JSON_DESCRIPTION, mDescription);
        json.put(JSON_FULL, mFull);
        //json.put(JSON_DATE, mDate.getTime());
        json.put(JSON_START_DATE, mStartDate.getTime());
        json.put(JSON_END_DATE, mEndDate.getTime());
        json.put(JSON_DEPOSIT_DUE, mDepositDue);
        json.put(JSON_DEPOSIT_DUE_DATE, mDepositDueDate.getTime());
        json.put(JSON_FEE_DUE, mFeeDue);
        json.put(JSON_FEE_DUE_DATE, mFeeDueDate.getTime());
        json.put(JSON_LIMIT, mLimit);

        return json;
    }

    @Override
    public String toString() {
        return mTitle;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public Date getmDate() {
        return mDate;
    }

    public Date getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(Date mStartDate) {
        this.mStartDate = mStartDate;
    }

    public Date getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(Date mEndDate) {
        this.mEndDate = mEndDate;
    }

    public String getmDepositDue() {
        return mDepositDue;
    }

    public void setmDepositDue(String mDepositDue) {
        this.mDepositDue = mDepositDue;
    }

    public Date getmDepositDueDate() {
        return mDepositDueDate;
    }

    public void setmDepositDueDate(Date mDepositDueDate) {
        this.mDepositDueDate = mDepositDueDate;
    }

    public String getmFeeDue() {
        return mFeeDue;
    }

    public void setmFeeDue(String mFeeDue) {
        this.mFeeDue = mFeeDue;
    }

    public Date getmFeeDueDate() {
        return mFeeDueDate;
    }

    public void setmFeeDueDate(Date mFeeDueDate) {
        this.mFeeDueDate = mFeeDueDate;
    }

    public int getmLimit() {
        return mLimit;
    }

    public void setmLimit(int mLimit) {
        this.mLimit = mLimit;
    }

    public boolean ismFull() {
        return mFull;
    }

    public void setmFull(boolean mFull) {
        this.mFull = mFull;
    }
}

