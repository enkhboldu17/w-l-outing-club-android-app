package edu.wlu.cs.outingclub.wloutingclub;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Stevan on 12/7/2016.
 * TODO: Change the appearance for days where there is a scheduled event
 */

public class CalendarActivity extends AppCompatActivity {
    CalendarView calendar;
    private TextView mOCEventDescription;
    private Button mOCEventButton;
    private List<OCEvent> mOCEvents;
    private static final String TAG = "CalendarActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //sets the main layout of the activity
        setContentView(R.layout.calendar_activity);

        mOCEventDescription = (TextView) findViewById(R.id.event_descrip);
        mOCEventButton = (Button) findViewById(R.id.calendar_event_button);
        mOCEventButton.setVisibility(View.GONE);
        EventLab eventLab = EventLab.get(this);
        mOCEvents = eventLab.getEvents();

        //initializes the calendarview
        initializeCalendar();
    }

    public void initializeCalendar() {
        calendar = (CalendarView) findViewById(R.id.calendar);

        // sets the first day of week according to Calendar.
        calendar.setFirstDayOfWeek(1);

        CalendarView calen = new CalendarView(this);

        Class<?> cvClass = calen.getClass();
        Field[] fields = cvClass.getDeclaredFields();
        for(Field field : fields) {
            String name = field.getName();

            Log.d(TAG, "Field name: " + name);
        }
        try {
            Field field = cvClass.getDeclaredField("DATE_FORMAT");
            field.setAccessible(true);
            ViewGroup tv = (ViewGroup) field.get(calendar);
            TextView k =  (TextView) tv.getChildAt(5);
            k.setTextColor(Color.RED);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

        //sets the listener to be notified upon selected date change.
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //show the selected date's scheduled event
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
                try{
                    for(int i=0; i < mOCEvents.size(); i++) {
                        if (sdf.parse((month + 1) + "-" + day + "-" + year).equals(mOCEvents.get(i).getmStartDate())) {

                            mOCEventDescription.setText(mOCEvents.get(i).getmDescription());
                            mOCEventButton.setVisibility(View.VISIBLE);
                            break;
                        }
                        else{
                            mOCEventDescription.setText(R.string.calendar_no_event);
                            mOCEventButton.setVisibility(View.GONE);

                        }
                    }
                } catch(ParseException e){
                    e.printStackTrace();
                }
            }
        });
    }
}
