package edu.wlu.cs.outingclub.wloutingclub.database;

/**
 * Created by michaelgarcia on 12/8/16.
 */

public class OCEventDbSchema {
    public static final class OCEventTable{
        public static final String NAME = "OCevents";

        public static final class Cols{
            public static final String ID = "id";
            public static final String Title = "title";
            public static final String Description = "description";
            public static final String Date = "date";
            public static final String StartDate = "startDate";
            public static final String EndDate = "endDate";
            public static final String DepositDue = "depositDue";
            public static final String DepositDueDate = "depositDueDate";
            public static final String FeeDue = "feeDue";
            public static final String FeeDueDate = "feeDueDate";
            public static final String Limit = "limit";
            public static final String Full = "false";
        }
    }
}
