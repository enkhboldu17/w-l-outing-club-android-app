package edu.wlu.cs.outingclub.wloutingclub;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventLab{
    private static EventLab sEventLab;
    private static String url = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCEvents";
    private static String getAllEnrollmentURL = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCEvents&Email=xxxxx@wlu.edu&Pin=######";
    private String TAG = EventLab.class.getSimpleName();
    private List<OCEvent> mOCEvents;

    public static EventLab get(Context context){
        if (sEventLab == null){
            sEventLab = new EventLab(context);
        }
        return sEventLab;
    }

    private EventLab(Context context){
        mOCEvents = new ArrayList<>();
        new GetOCEvents().execute();
    }


    class GetOCEvents extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray OCEvents = jsonObj.getJSONArray("OCEvents");

                    // looping through All Events
                    for (int i = 0; i < OCEvents.length(); i++) {
                        JSONObject o = OCEvents.getJSONObject(i);

                        try {
                            OCEvent ocEvent = new OCEvent(o);

                            // adding event to the event list
                            mOCEvents.add(ocEvent);
                        } catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                } catch (final JSONException e) {
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
            }
            return null;
        }

    }

    public List<OCEvent> getEvents(){
        return mOCEvents;
    }

    public OCEvent getOCEvent(int id){
        for (OCEvent ocEvent : mOCEvents){
            if (ocEvent.getmId() == (id)){
                return ocEvent;
            }
        }
        return null;
    }
}