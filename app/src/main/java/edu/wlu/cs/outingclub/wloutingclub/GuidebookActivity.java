package edu.wlu.cs.outingclub.wloutingclub;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by brandon on 12/7/16.
 */

public class GuidebookActivity extends AppCompatActivity{

    private Button forewordButton;
    private Button planningTripButton;
    private Button leaveNoTraceButton;
    private Button firstAidButton;
    private Button hikingButton;
    private Button climbButton;
    private Button canoeKayakButton;
    private Button mountainBikeButton;
    private Button huntingButton;
    private Button fishingButton;
    private Button cyclingButton;
    private Button cavingButton;
    private Button skiingButton;
    private Button flowersButton;
    private Button constellationsButton;
    private Button skylarkButton;
    private Button sustainButton;
    private Button indexButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guidebook);

        forewordButton = (Button) findViewById(R.id.foreword);
        forewordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCforeword.pdf"));
                startActivity(i);
            }
        });

        planningTripButton = (Button) findViewById(R.id.planning_trip);
        planningTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCplanning.pdf"));
                startActivity(i);
            }
        });

        leaveNoTraceButton = (Button) findViewById(R.id.leave_no_trace);
        leaveNoTraceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCleaveNoTrace.pdf"));
                startActivity(i);
            }
        });

        firstAidButton = (Button) findViewById(R.id.first_aid);
        firstAidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCwildernessFirstAid.pdf"));
                startActivity(i);
            }
        });

        hikingButton = (Button) findViewById(R.id.hiking);
        hikingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCHiking.pdf"));
                startActivity(i);
            }
        });

        climbButton = (Button) findViewById(R.id.climb);
        climbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCclimbing.pdf"));
                startActivity(i);
            }
        });

        canoeKayakButton = (Button) findViewById(R.id.canoe_kayak);
        canoeKayakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCcanoeAndKayak.pdf"));
                startActivity(i);
            }
        });

        mountainBikeButton = (Button) findViewById(R.id.mountain_biking);
        mountainBikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCmountainBiking.pdf"));
                startActivity(i);
            }
        });

        huntingButton = (Button) findViewById(R.id.hunting);
        huntingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OChunting.pdf"));
                startActivity(i);
            }
        });

        fishingButton = (Button) findViewById(R.id.fishing);
        fishingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCfishing.pdf"));
                startActivity(i);
            }
        });

        cyclingButton = (Button) findViewById(R.id.cycling);
        cyclingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCcycling.pdf"));
                startActivity(i);
            }
        });

        cavingButton = (Button) findViewById(R.id.caving);
        cavingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCcaving.pdf"));
                startActivity(i);
            }
        });

        skiingButton = (Button) findViewById(R.id.skiing);
        skiingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCskiing.pdf"));
                startActivity(i);
            }
        });

        flowersButton = (Button) findViewById(R.id.flowers_shrubs_trees);
        flowersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCplants.pdf"));
                startActivity(i);
            }
        });

        constellationsButton = (Button) findViewById(R.id.constellations);
        constellationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCconstellations.pdf"));
                startActivity(i);
            }
        });

        skiingButton = (Button) findViewById(R.id.skiing);
        skiingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCskiing.pdf"));
                startActivity(i);
            }
        });

        skylarkButton = (Button) findViewById(R.id.skylark);
        skylarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCskylark.pdf"));
                startActivity(i);
            }
        });

        sustainButton = (Button) findViewById(R.id.sustain);
        sustainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCsustainability.pdf"));
                startActivity(i);
            }
        });

        indexButton = (Button) findViewById(R.id.index);
        indexButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://managementtools3.wlu.edu/OutingClub/docs/OCindex.pdf"));
                startActivity(i);
            }
        });

    }

}
