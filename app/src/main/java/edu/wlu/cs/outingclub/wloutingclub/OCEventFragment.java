package edu.wlu.cs.outingclub.wloutingclub;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by michaelgarcia on 12/8/16.
 */

//public class OCEventFragment extends Fragment{
public class OCEventFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    private OCEvent mOCEvent;
    private Button mSignUpButton;
    private List<OCEvent> mOCEvents;
    private TextView mTitle;
    private TextView mDescription;
    private Spinner fee;
    private Spinner deposit;
    private EditText userNametxt;
    private EditText pINtxt;

    private static final String getPin = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCPin&Email=";
    private static final String genPin = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCNewPin&Email=";

    private static final String TAG = "OCEventFragment";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int ocEventID = getArguments().getInt("edu.wlu.cs.outingclub.wloutingclub.ocevent_id");
        EventLab eventLab = EventLab.get(getActivity());
        mOCEvents = eventLab.getEvents();
        Log.d("OCEVENTID", ""+ocEventID);
        for (int i=0; i<mOCEvents.size();i++){
            Log.d("EventFragmentCreation", "" +mOCEvents.get(i).getmId());
            if (ocEventID==(mOCEvents.get(i).getmId())){

                mOCEvent = mOCEvents.get(i);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ocevent, container, false);
        //final HttpHandler h = new HttpHandler();

        mTitle = (TextView) v.findViewById(R.id.oc_event_title);
        mTitle.setText(mOCEvent.getmTitle());

        mDescription = (TextView) v.findViewById(R.id.oc_event_description);
        mDescription.setText(mOCEvent.getmDescription());


        mSignUpButton = (Button) v.findViewById(R.id.oc_single_event_signup);
        mSignUpButton.setEnabled(!mOCEvent.ismFull());
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog login = new Dialog(OCEventFragment.this.getActivity());
                login.setContentView(R.layout.login_dialog);
                login.setTitle("Login");

                Button loginButton = (Button) login.findViewById(R.id.btnLogin);
                Button cancelButton = (Button) login.findViewById(R.id.btnCancel);
                Button genButton = (Button) login.findViewById(R.id.genPin);
                Button getButton = (Button) login.findViewById(R.id.getPin);


                deposit = (Spinner) login.findViewById(R.id.deposit);

                ArrayAdapter<CharSequence> depAdapter = ArrayAdapter.createFromResource(getContext(),
                        R.array.deposit, android.R.layout.simple_spinner_item);
                depAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                deposit.setAdapter(depAdapter);

                fee = (Spinner) login.findViewById(R.id.fee);

                ArrayAdapter<CharSequence> feeAdapter = ArrayAdapter.createFromResource(getContext(),
                        R.array.fee, android.R.layout.simple_spinner_item);
                feeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                fee.setAdapter(feeAdapter);


                userNametxt = (EditText) login.findViewById(R.id.txtEmail);
                pINtxt = (EditText) login.findViewById(R.id.txtPin);

                loginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(userNametxt.getText().toString()).matches()) {
                            Toast.makeText(OCEventFragment.this.getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
                        } else if (pINtxt.getText().toString().trim().length() != 8) {
                            Toast.makeText(OCEventFragment.this.getActivity(), "Please enter 8 digit PIN", Toast.LENGTH_SHORT).show();
                        } else {
                            String inputString = new String("http://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCEnroll&EventID="
                                    + mOCEvent.getmId() + "&DepositPaymentType=" + deposit.getSelectedItem().toString() + "&FeePaymentType=" +
                                    fee.getSelectedItem().toString() + "&Email=" + userNametxt.getText() + "&Pin=" + pINtxt.getText());
                            new EnrollInEvent().execute(inputString);
                            login.dismiss();
                        }
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        login.dismiss();
                    }
                });

                genButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(userNametxt.getText().toString()).matches()) {
                            Toast.makeText(OCEventFragment.this.getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
                        } else {
                            String inputString = new String(genPin + userNametxt.getText().toString());
                            //Log.d("attempt 2 append 2 url", inputString);
                            new GeneratePIN().execute(inputString);
                            Toast.makeText(OCEventFragment.this.getActivity(), "A new PIN has been sent to your email", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                getButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(userNametxt.getText().toString()).matches()) {
                            Toast.makeText(OCEventFragment.this.getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
                        } else {
                            String inputString = new String(getPin + userNametxt.getText().toString());
                            //Log.d("attempt 2 append 2 url", inputString);
                            new GeneratePIN().execute(inputString);
                            Toast.makeText(OCEventFragment.this.getActivity(), "Your PIN has been sent to your email", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                login.show();

            }
        });

        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class EnrollInEvent extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... inputString) {
            HttpHandler httpHandler = new HttpHandler();
            String jsonStr = httpHandler.makeServiceCall(inputString[0]);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                } catch (final JSONException e) {
                }
            } else {
                Log.e("LALA", "Couldn't get json from server.");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(OCEventFragment.this.getActivity(), "You are now enrolled in " + mOCEvent.getmTitle(), Toast.LENGTH_SHORT).show();
        }
    }


    private class GeneratePIN extends AsyncTask<String, Void, Void> {
        protected Void doInBackground(String... inputString) {
            HttpHandler httpHandler = new HttpHandler();
            String jsonStr = httpHandler.makeServiceCall(inputString[0]);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                } catch (final JSONException e) {
                }
            } else {
                Log.e("LALA", "Couldn't get json from server.");
            }

            return null;
        }

    }
}
