package edu.wlu.cs.outingclub.wloutingclub;

import android.content.Intent;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by michaelgarcia on 12/7/16.
 */

public class OCEventListFragment extends Fragment {

    private RecyclerView mOCEventRecyclerView;
    private OCEventAdapter mAdapter;
    private static String getAllURL = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCEvents";
    private static String getAllEnrollmentURL = "https://managementtools3.wlu.edu/OutingClub/JSON.aspx?Get=OCEvents&Email=xxxxx@wlu.edu&Pin=######";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_oc_event_list, container, false);

        mOCEventRecyclerView = (RecyclerView) view.findViewById(R.id.OC_events_recycler);
        mOCEventRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }

    private void updateUI(){
        EventLab eventLab = EventLab.get(getActivity());
        List<OCEvent> ocEvents = eventLab.getEvents();
        mAdapter = new OCEventAdapter(ocEvents);
        mOCEventRecyclerView.setAdapter(mAdapter);
    }

    private class OCEventHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private OCEvent mOCEvent;

        public OCEventHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_ocevent_title_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_ocevent_date_text_view);
        }

        public void bindOCEvent(OCEvent ocEvent){
            mOCEvent = ocEvent;
            mTitleTextView.setText(mOCEvent.getmTitle());
            mDateTextView.setText(mOCEvent.getmStartDate().toString());
        }

        @Override
        public void onClick(View v){
            Intent intent = OCEventActivity.newIntent(getActivity(), mOCEvent.getmId());
            startActivity(intent);
        }
    }

    private class OCEventAdapter extends RecyclerView.Adapter<OCEventHolder>{

        private List<OCEvent> mOCEvents;

        public OCEventAdapter(List<OCEvent> ocEvents) {
            mOCEvents = ocEvents;
        }

        @Override
        public OCEventHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_ocevent, parent, false);
            return new OCEventHolder(view);
        }

        @Override
        public void onBindViewHolder(OCEventHolder holder, int position){
            OCEvent ocEvent = mOCEvents.get(position);
            holder.bindOCEvent(ocEvent);
        }

        @Override
        public int getItemCount(){
            return mOCEvents.size();
        }
    }
}