package edu.wlu.cs.outingclub.wloutingclub;

import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by michaelgarcia on 12/8/16.
 */

public class OCEventListActivity extends SingleEventActivity{

    static final String TAG = "OCEventListActivity";
    @Override
    protected Fragment createFragment(){
        Log.d(TAG, "createFragment: new OC Event List Fragment returned");
        return new OCEventListFragment();
    }
}
